//
//  MatchDetailsViewController.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 16.01.2016.
//  Copyright © 2016 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit


class MatchDetailsViewController : UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var match : Match!
    
    @IBOutlet var localTeamImageView : UIImageView!
    @IBOutlet var visitorTeamImageView : UIImageView!
    @IBOutlet var timeLabel : UILabel!
    @IBOutlet var scoreLabel : UILabel!
    @IBOutlet var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = match.localTeamName!.uppercaseString + " vs. " + match.visitorName!.uppercaseString
        self.navigationItem.backBarButtonItem?.title = ""
        self.setAppearance()
    }
    
    
    func setAppearance(){
        self.visitorTeamImageView.layer.cornerRadius = CGRectGetHeight(self.visitorTeamImageView.frame)/2
        self.visitorTeamImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.visitorTeamImageView.layer.borderWidth = 1.0
        self.visitorTeamImageView.clipsToBounds = true
        
        self.localTeamImageView.layer.cornerRadius = CGRectGetHeight(self.localTeamImageView.frame)/2
        self.localTeamImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.localTeamImageView.layer.borderWidth = 1.0
        self.localTeamImageView.clipsToBounds = true
        
        self.localTeamImageView.image = UIImage(named: match.localTeamName!)
        self.visitorTeamImageView.image = UIImage(named: match.visitorName!)
        
        
        self.timeLabel.text = self.match.matchDay
        
        self.scoreLabel.text = (match.finalScore != "") ? match.finalScore  : "FUTURE"
        
    }
    
    //MARK - UITableView Data Source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return match.matchEventList!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let matchEvent = match.matchEventList![indexPath.row]
        let eventCell : EventCell!
        if matchEvent.teamType == TeamType.LocalTeam {
            eventCell = tableView.dequeueReusableCellWithIdentifier("LocalEventCell") as! LocalEventCell
        } else {
            eventCell = tableView.dequeueReusableCellWithIdentifier("VisitorEventCell") as! VisitorEventCell
        }
        eventCell.configureCell(matchEvent)
        return eventCell
    }
    
}



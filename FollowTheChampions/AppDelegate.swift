//
//  AppDelegate.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 03.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UIAlertViewDelegate {

    var window: UIWindow?
    var pushToken : String?
    //let demoToken = "demoToken"
    //let demoToken = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF"

    class func getPushToken() -> String? {
        return (UIApplication.sharedApplication().delegate as! AppDelegate).pushToken
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        setupAppearance()
        
        if UIApplication.sharedApplication().respondsToSelector(Selector("registerUserNotificationSettings:")) {
           // UIApplication.sharedApplication().registerForRemoteNotifications()
            let settings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func setupAppearance(){
        self.window?.backgroundColor = UIColor.whiteColor()
        UINavigationBar.appearance().backgroundColor = UIColor.whiteColor()
        UINavigationBar.appearance().barTintColor = UIColor.whiteColor()
        UINavigationBar.appearance().tintColor = UIColor.darkBlueColor()
        UINavigationBar.appearance().titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor.darkBlueColor(),
                NSFontAttributeName: UIFont(name: "RobotoCondensed-Regular", size: 20)!]
        UINavigationBar.appearance().alpha = 1.0
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        UITabBar.appearance().barTintColor = UIColor.whiteColor()
        UITabBar.appearance().backgroundColor = UIColor.whiteColor()
        UITabBar.appearance().tintColor = UIColor.darkBlueColor()
        UITabBar.appearance().translucent = false
        UITabBar.appearance().alpha = 1.0
        
    }
    
    //MARK: Push notifications
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if UIApplication.sharedApplication().respondsToSelector(Selector("registerUserNotificationSettings:")) {
            UIApplication.sharedApplication().registerForRemoteNotifications()
        }
    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        var token = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
        token = (token.componentsSeparatedByString(" ") as NSArray).componentsJoinedByString("")
        self.pushToken = token
        self.window?.rootViewController = TabBarViewController()
        ApiController.registerPushToken(token) { (success, error) -> Void in
            
        }
        
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print(userInfo)
        if let payload = userInfo["payload"] {
            if let type = payload["type"], player = payload["player"], time = payload["time"] {
                var aps = payload["aps"] as! [String:String]
                var alert = aps["alert"]
                alert = alert?.substringToIndex((alert?.characters.startIndex.advancedBy((alert?.characters.count)! - (type as! String).characters.count))!)
                var message = ""
                if type as! String == "green card" ||  type as! String == "redcard" || type as! String == "yellowcard" {
                    message = (player as! String) + " received " + (type as! String)
                    message = message + " at " + (time as! String) + " minute!"
                    
                } else if type as! String == "goal" {
                    message = (player as! String) + " scored a " + (type as! String)
                    message = message + " at " + (time as! String) + " minute!"
                    
                }
                UIAlertView(title: alert, message: message, delegate: nil, cancelButtonTitle: "OK").show()
            }
        }
        
    }

}


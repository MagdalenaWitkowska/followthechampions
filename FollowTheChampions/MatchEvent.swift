//
//  MatchEvent.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 06.12.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

/*
{
    id = 19625621;
    minute = 12;
    playerName = "R. van Persie";
    result = "[0 - 1]";
    type = goal;
    whichTeam = visitorteam;
}
*/

import Foundation
import ObjectMapper
import UIKit

enum MatchEventType : String {
    case Goal = "goal"
    case RedCard = "redcard"
    case Yellowcard = "yellowcard"
}

enum TeamType : String {
    case VisitorTeam = "visitorteam"
    case LocalTeam = "localteam"
}


class MatchEvent : Mappable {
    

    var eventId : Int?
    var minute  : String?
    var player  : String?
    var result  : String?
    var type    : MatchEventType?
    var teamType: TeamType?
    
    private var typeString : String?
    private var teamTypeString : String?
    
    required init?(_ map: Map){
        mapping(map)
    }
    
    
    func mapping(map: Map) {
        eventId    <- map["id"]
        minute     <- map["minute"]
        player     <- map["playerName"]
        result     <- map["result"]
        typeString <- map["type"]
        teamTypeString   <- map["whichTeam"]
        
        self.type = MatchEventType(rawValue: typeString!)
        self.teamType = TeamType(rawValue: teamTypeString!)
        
    }
    
 }
//
//  FavouritesViewController.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 15.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit

class FavouritesViewController : UITableViewController {
    
    var favMatches : [Match] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let titleView = NSBundle.mainBundle().loadNibNamed("NavigationTitleView", owner: self, options: nil).first as? NavigationTitleView {
            titleView.titleLabel.text = "FAVOURITES"
            self.navigationController?.navigationBar.topItem?.titleView = titleView
        }
        

    

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        ApiController.getLikedMatches(AppDelegate.getPushToken()!) { (success, error, object) -> Void in
            if success {
                self.favMatches = object!
                for var i = 0; i < self.favMatches.count; i++ {
                    if self.favMatches[i].matchIndex! < 9000 || self.favMatches[i].localTeamName == nil || self.favMatches[i].visitorName == nil {
                        self.favMatches.removeAtIndex(i)
                        i--
                    }
                }
                self.favMatches = self.favMatches.sort({
                    ($0.matchDate! as NSDate).compare($1.matchDate! as NSDate) == NSComparisonResult.OrderedDescending
                })
                self.tableView.reloadData()
            }
            
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let matchCell = tableView.dequeueReusableCellWithIdentifier("matchCell") as! MatchCell
        matchCell.configureMatchCell(favMatches[indexPath.row])
        return matchCell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favMatches.count
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let match = favMatches[indexPath.row]
        if (match.finalScore != "") {
            let matchDetails = UIStoryboard(name: "MatchDetailsViewController", bundle: nil).instantiateInitialViewController() as! MatchDetailsViewController
            matchDetails.match = match
            self.navigationController?.pushViewController(matchDetails, animated: true)
        }
    }
}
//
//  ApiController.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 25.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
import Alamofire

class ApiController {
   // private let baseURL: String = "http://192.168.0.13:8080"
   private let baseURL: String = "http://MacBook-Pro-Magdalena.local:8080"
    //private let baseURL: String = "http://10.20.117.110:8080"
    
    class var sharedInstance : ApiController {
        struct Static {
            static let instance : ApiController = ApiController()
        }
        return Static.instance
        
    }
    
    class func getTeams(token: String!, handler: ((success: Bool, error: NSError?, object: [Team]?) -> Void)) {
        
        let path = "/mobile/getStandingsPersonalized?deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.GET, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                if responseObject.isSuccess == true {
                    if let JSON = responseObject.value {
                        handler(success: true, error: nil, object: Mapper<Team>().mapArray(JSON))
                    }
                } else if let error = responseObject.error as? NSError {
                    handler(success: false, error: error, object: nil)
                }
        }

        
    }
    
    
    class func getMatches(token : String, handler: ((success: Bool, error: NSError?, object: [Match]?) -> Void)) {
        
        let path = "/mobile/searchForMatchesPersonalized?deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.GET, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
                
                if responseObject.isSuccess == true {
                    if let JSON = responseObject.value{
                        handler(success: true, error: nil, object: Mapper<Match>().mapArray(JSON))
                    }
                } else if let error = responseObject.error as? NSError {
                    handler(success: false, error: error, object: nil)
                }
        }
        
        
    }
    
    class func registerPushToken(token : String!, handler: ((success: Bool, error: NSError?) -> Void)) {
    
        let path = "/mobile/registerDevice?deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.POST, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
        }
    
    }
    
    class func likeTeam(token : String!, teamId : Int!, handler: ((success: Bool, error: NSError?) -> Void)) {
        
        let path = "/mobile/addFavouritedTeam?teamId=\(teamId)" + "&deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.POST, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
        }
        
    }
    
    class func unlikeTeam(token : String!, teamId : Int!, handler: ((success: Bool, error: NSError?) -> Void)) {
        
        let path = "/mobile/removeFavouritedTeam?teamId=\(teamId)" + "&deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.POST, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
        }
        
    }
    
    
    class func likeMatch(token : String!, matchId : Int!, handler: ((success: Bool, error: NSError?) -> Void)) {
        
        let path = "/mobile/addFavouritedMatch?matchId=\(matchId)" + "&deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.POST, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
        }
        
    }
    
    class func unlikeMatch(token : String!, matchId : Int!, handler: ((success: Bool, error: NSError?) -> Void)) {
        
        let path = "/mobile/removeFavouritedMatch?matchId=\(matchId)" + "&deviceToken=" + token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.POST, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
        }
        
    }
    
    class func getLikedTeams(token : String!, handler: ((success: Bool, error: NSError?, object: [Match]?) -> Void)) {
        
        let path = "/mobile/getFavouritedTeams?deviceToken="+token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.GET, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
                
                if responseObject.isSuccess == true {
                    if let JSON = responseObject.value {
                        handler(success: true, error: nil, object: Mapper<Match>().mapArray(JSON))
                    }
                } else if let error = responseObject.error as? NSError {
                    handler(success: false, error: error, object: nil)
                }
        }
    }
    
    class func getLikedMatches(token: String!, handler: ((success: Bool, error: NSError?, object: [Match]?) -> Void)) {
        
        let path = "/mobile/getAllFavouritedMatches?deviceToken="+token
        let requestURL = self.sharedInstance.baseURL.stringByAppendingString(path)
        
        Alamofire.request(.GET, requestURL, parameters: [:])
            .responseJSON { (request, response, responseObject) -> Void in
                print(responseObject.value)   // result of response serialization
                
                
                if responseObject.isSuccess == true {
                    if let JSON = responseObject.value {
                        handler(success: true, error: nil, object: Mapper<Match>().mapArray(JSON))
                    }
                } else if let error = responseObject.error as? NSError {
                    handler(success: false, error: error, object: nil)
                }
        }
        
        
    }
}



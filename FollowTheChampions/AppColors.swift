//
//  AppColors.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 14.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    class func darkBlueColor() -> UIColor {
        return UIColor(red: 0/255.0, green: 35.0/255.0, blue: 93.0/255.0, alpha: 1.0)
    }
}
//
//  Match.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 25.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper


/*
{
id = 1962562;
localTeam =         {
id = 9363;
name = Southampton;
};
matchDate = "08.12.2014";
matchFtScore = "[1-2]";
matchHtScore = "[1-1]";
status = FT;
time = "20:00";
visitorTeam =         {
id = 9260;
name = "Manchester United";
};
},
*/

class Match : Mappable {
    
    var matchIndex    : Int?
    var localTeamId   : Int?
    var localTeamName : String?
    var visitorTeamId : Int?
    var visitorName   : String?
    var finalScore    : String?
    var localGoals    : Int?
    var visitorGoals  : Int?
    var halftimeScore : String?
    var matchStatus   : String?
    var matchEventList: [MatchEvent]?
    var matchDate     : NSDate?
    var isFavourite   : Bool?
    
    
    var matchDay    : String?
    private var matchTime   : String?
    
    var localGoalsString : String! = ""
    var visitorGoalsString : String! = ""
    
    
    required init?(_ map: Map){
        mapping(map)
    }
    
    
    func mapping(map: Map) {
        matchIndex      <- map["id"]
        localTeamId     <- map["localTeam.id"]
        localTeamName   <- map["localTeam.name"]
        visitorTeamId   <- map["visitorTeam.id"]
        visitorName     <- map["visitorTeam.name"]
        matchEventList  <- map["matchEventList"]
        matchStatus     <- map["status"]
        finalScore      <- map["matchFtScore"]
        halftimeScore   <- map["matchHtScore"]
        matchDay        <- map["matchDate"]
        matchTime       <- map["time"]
        isFavourite     <- map["isFavourite"]
        //let dateFormatter = NSDateFormatter()
        //matchDate = dateFormatter.dateFromString(matchTime! + " " + matchDay!)
        
        if finalScore != nil {
            finalScore = String(finalScore!.characters.dropFirst())
            finalScore = String(finalScore!.characters.dropLast())
        }
        
        
        //let range = finalScore!.rangeOfString("-")
        localGoalsString = ""
        visitorGoalsString = ""
        
        var localDictionary : [String : [String]] = [:]
        var visitorDictionary : [String : [String]] = [:]
        
        if matchEventList != nil {
            for matchEvent in matchEventList! {
                if matchEvent.type == MatchEventType.Goal {
                    if matchEvent.teamType == TeamType.LocalTeam {
                        if localDictionary[matchEvent.player!] != nil {
                            localDictionary[matchEvent.player!]!.append(matchEvent.minute!)
                        } else {
                            localDictionary.updateValue([matchEvent.minute!], forKey: matchEvent.player!)
                        }
                    }
                    if matchEvent.teamType == TeamType.VisitorTeam {
                        if visitorDictionary[matchEvent.player!] != nil {
                            visitorDictionary[matchEvent.player!]!.append(matchEvent.minute!)
                        } else {
                            visitorDictionary.updateValue([matchEvent.minute!], forKey: matchEvent.player!)
                        }
                    }
                    
                }
            }
        }
        
        localGoalsString = ""
        for (str, val) in localDictionary {
            localGoalsString = localGoalsString + str
            var minutesStr = ""
            for minute in val {
                minutesStr = minutesStr + " " + minute + "' "
            }
            localGoalsString = localGoalsString + " " + minutesStr + "\n"
        }
        localGoalsString = (localGoalsString != "") ? localGoalsString.substringToIndex(localGoalsString.endIndex.predecessor().predecessor()) : ""
        visitorGoalsString = ""
        for (str, val) in visitorDictionary {
            visitorGoalsString = visitorGoalsString + str
            var minutesStr = ""
            for minute in val {
                minutesStr = minutesStr + " " + minute + "' "
            }
            visitorGoalsString = visitorGoalsString + " " + minutesStr + "\n"
        }
        visitorGoalsString =  (visitorGoalsString  != "") ? visitorGoalsString.substringToIndex(visitorGoalsString.endIndex.predecessor().predecessor()) : ""
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd.mm.yy"
        if matchDay != nil {
            matchDate = dateFormatter.dateFromString(matchDay!)
        }
        
    }
    
}

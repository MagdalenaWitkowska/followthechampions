//
//  RankingCell.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 16.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit

class RankingCell : UITableViewCell {
 
    @IBOutlet var numberView : UIView!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var logoImageView : UIImageView!
    @IBOutlet var likeImageView : UIImageView!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var matchesLabel: UILabel!
    @IBOutlet var pointsLabel : UILabel!
    @IBOutlet var winsLabel : UILabel!
    @IBOutlet var losesLabel : UILabel!
    @IBOutlet var drawsLabel : UILabel!
    @IBOutlet var likeTeamButton : UIButton!
    
    var team : Team!
    
    @IBAction func likeTeam(){
        self.likeImageView.image = UIImage(named: (team.isFavourite == false) ? "like_active" : "like_inactive")
        if team.isFavourite == true {
            self.team.isFavourite = false
            ApiController.unlikeTeam(AppDelegate.getPushToken(), teamId: team.teamId!, handler: { (success, error) -> Void in
                
            })
        } else {
            self.team.isFavourite = true
            ApiController.likeTeam(AppDelegate.getPushToken(), teamId: team.teamId!, handler: { (success, error) -> Void in
                
            })
        }
    }

}
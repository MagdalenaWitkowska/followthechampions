//
//  Team.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 25.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

/*
{
    id = 12049287;
    standDesc = "Relegation - Championship";
    standOverallD = 4;
    standOverallGa = 30;
    standOverallGp = 14;
    standOverallGs = 14;
    standOverallL = 8;
    standOverallW = 2;
    standPoints = 10;
    standPosition = 19;
    standRound = "<null>";
    standTeamId = 9287;
    standTeamName = "Newcastle Utd";
}
*/


class Team : Mappable {
    
    var standingDesc    : String?
    var teamId          : Int?
    var standPoints     : String?
    var matchesPlayed   : String?
    var wins            : String?
    var draws           : String?
    var loses           : String?
    var goalsWon        : String?
    var goalsLost       : String?
    var points          : String?
    var rankingIndex    : String?
    var name            : String?
    var isFavourite     : Bool = false
    
    
    required init?(_ map: Map){
        mapping(map)
    }
    
    
    func mapping(map: Map) {
        rankingIndex    <- map["standPosition"]
        name            <- map["standTeamName"]
        standingDesc    <- map["standDesc"]
        teamId          <- map["standTeamId"]
        standPoints     <- map["standPoints"]
        matchesPlayed   <- map["standOverallGp"]
        wins            <- map["standOverallW"]
        draws           <- map["standOverallD"]
        loses           <- map["standOverallL"]
        goalsWon        <- map["standOverallGs"]
        goalsLost       <- map["standOverallGa"]
        points          <- map["standPoints"]
        isFavourite     <- map["isFavourite"]

    }
}


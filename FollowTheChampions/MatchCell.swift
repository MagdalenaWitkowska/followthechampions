//
//  MatchCell.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 16.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import Foundation
import UIKit

class MatchCell : UITableViewCell {
    
    @IBOutlet var nowTimerView : UIView!
    
    @IBOutlet weak var visitorTeamImageView: UIImageView!
    @IBOutlet weak var localTeamImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeNowLabel: UILabel!
    @IBOutlet weak var visitorScoreLabel: UILabel!
    @IBOutlet weak var localScoreLabel: UILabel!
    @IBOutlet weak var visitorEventsLabel: UILabel!
    @IBOutlet weak var localEventsLabel: UILabel!
    @IBOutlet weak var localTeamLabel: UILabel!
    @IBOutlet weak var visitorTeamLabel: UILabel!
    @IBOutlet weak var scoreLabel  : UILabel!
    @IBOutlet weak var likedImage : UIImageView!
    
    var match : Match!
    func configureMatchCell(match : Match){
        self.match = match
        self.visitorTeamImageView.layer.cornerRadius = CGRectGetHeight(self.visitorTeamImageView.frame)/2
        self.visitorTeamImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.visitorTeamImageView.layer.borderWidth = 1.0
        
        self.localTeamImageView.layer.cornerRadius = CGRectGetHeight(self.localTeamImageView.frame)/2
        self.localTeamImageView.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.localTeamImageView.layer.borderWidth = 1.0
        self.localTeamImageView.clipsToBounds = true
        
        self.localTeamImageView.image = UIImage(named: match.localTeamName!)
        self.visitorTeamImageView.image = UIImage(named: match.visitorName!)
        self.localTeamLabel.text = match.localTeamName
        self.visitorTeamLabel.text = match.visitorName
        self.visitorTeamImageView.clipsToBounds = true
        
        self.scoreLabel.text = (match.finalScore != "") ? match.finalScore  : "FUTURE"
        
        self.localEventsLabel.text = match.localGoalsString
        self.visitorEventsLabel.text = match.visitorGoalsString
        
        self.timeNowLabel.text = match.matchDay
        if likedImage != nil {
            self.likedImage.image = UIImage(named: (match.isFavourite == true) ? "like_active" : "like_inactive")
        }
        
        //self.dateLabel.text = match.matchDay
    }
    
    @IBAction func likeMatch(){
        if likedImage != nil {
            self.likedImage.image = UIImage(named: (match.isFavourite == false) ? "like_active" : "like_inactive")
        }
        
        if match.isFavourite == true {
            self.match.isFavourite = false
            ApiController.unlikeMatch(AppDelegate.getPushToken(), matchId: match.matchIndex, handler: { (success, error) -> Void in
                
            })
        } else {
            self.match.isFavourite = true
            ApiController.likeMatch(AppDelegate.getPushToken(), matchId: match.matchIndex!, handler: { (success, error) -> Void in
                
            })
        }
    }
}
//
//  MatchDetailsCell.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 16.01.2016.
//  Copyright © 2016 Magdalena Witkowska. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit


class EventCell : UITableViewCell {
    
    @IBOutlet var timeLabel : UILabel!
    @IBOutlet var playersName : UILabel!
    @IBOutlet var eventImageView : UIImageView!
    
    var teamName : String!
    
    func configureCell(matchEvent : MatchEvent){
        self.timeLabel.text = matchEvent.minute! + "'"
        self.playersName.text = matchEvent.player
        
        switch matchEvent.type! {
        case .Goal:
            self.eventImageView.image = UIImage(named:"goal")
        case .Yellowcard:
            self.eventImageView.image = UIImage(named:"yellowcard")
        case .RedCard:
            self.eventImageView.image = UIImage(named:"redcard")
        }
        
    }
}


//
//  ViewController.swift
//  FollowTheChampions
//
//  Created by Magdalena Witkowska on 03.11.2015.
//  Copyright © 2015 Magdalena Witkowska. All rights reserved.
//

import UIKit

class TabBarViewController : UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let rankingViewController = UIStoryboard(name: "RankingViewController", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let matchesViewController = UIStoryboard(name: "MatchesViewController", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let favouritesViewController = UIStoryboard(name: "FavouritesViewController", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let rankingTabBarItem = UITabBarItem(title: "", image: UIImage(named: "ranking"), selectedImage: UIImage(named: "ranking_filled"))
        rankingTabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0)
        let matchesTabBarItem = UITabBarItem(title: "", image: UIImage(named: "ball"), selectedImage: UIImage(named: "ball_filled"))
        matchesTabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0)
        let favouritesTabBarItem = UITabBarItem(title: "", image: UIImage(named: "heart"), selectedImage: UIImage(named: "heart_filled"))
        favouritesTabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0)

        rankingViewController.tabBarItem = rankingTabBarItem
        matchesViewController.tabBarItem = matchesTabBarItem
        favouritesViewController.tabBarItem = favouritesTabBarItem

        self.setViewControllers([rankingViewController, matchesViewController, favouritesViewController], animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

